package com.example.minipos;

import java.text.NumberFormat;
import java.util.Locale;

public class Utils {
    String formatRupiah(String text){
        Integer nominal = Integer.getInteger(text);
        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        return  format.format(nominal);
    }
}
