package com.example.minipos;

public class Constants {
    public static final String HASILMENUCATEGORY = "hasil";
    public static final String IDCATEGORY = "id_category";
    public static final String DETAILCATEGORY = "detail_category";
    public static final String IDDETAILPRODUCT = "id_detail_product";
}
