package com.example.minipos.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.minipos.Constants;
import com.example.minipos.R;
import com.example.minipos.core.RetrofitClientInstance;
import com.example.minipos.core.network.response.product.DataResponse;
import com.example.minipos.core.network.response.product.ProductsResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProductActivity extends AppCompatActivity {
    TextView tvDetailName, tvDetailPrice, tvDetailDesc;
    ImageView detailImage;

    DataResponse dataDetailResponse;
    String idDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        dataDetailResponse = getIntent().getParcelableExtra(Constants.DETAILCATEGORY);
        idDetail = getIntent().getStringExtra(Constants.IDDETAILPRODUCT);

        tvDetailName = findViewById(R.id.tvNameDetail);
        tvDetailPrice = findViewById(R.id.tvPriceDetail);
        tvDetailDesc = findViewById(R.id.tvDescDetail);
        detailImage = findViewById(R.id.imgDetail);


        if (getIntent().getExtras() != null){
            Call<ProductsResponse> productsResponseDetailCall = RetrofitClientInstance.getApiService().getDataDetailProduct(idDetail);
            productsResponseDetailCall.enqueue(new Callback<ProductsResponse>() {
                @Override
                public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                    Gson gson = new Gson();
                    Log.d("detail product ", gson.toJson(response.body()));
                }

                @Override
                public void onFailure(Call<ProductsResponse> call, Throwable t) {

                }
            });
        }
    }
}