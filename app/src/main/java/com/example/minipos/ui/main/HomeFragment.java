package com.example.minipos.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import com.example.minipos.R;
import com.example.minipos.core.RetrofitClientInstance;
import com.example.minipos.core.adapter.CategoryAdapter;
import com.example.minipos.core.adapter.MenuCategoryAdapter;
import com.example.minipos.core.adapter.ProductsAdapter;
import com.example.minipos.core.network.response.product.DataResponse;
import com.example.minipos.core.network.response.product.ProductsResponse;
import com.example.minipos.core.network.response.product.category.CategoryResponse;
import com.example.minipos.core.network.response.product.category.DataCategoryResponse;
import com.google.gson.Gson;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;

    RecyclerView recyclerViewCategory;
    LinearLayoutManager linearLayoutManager;

    ProductsAdapter pAdapter;
    CategoryAdapter cAdapter;

    List<DataResponse> dataResponseList;
    List<DataCategoryResponse> dataCategoryResponseList;

    SearchView searchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        searchView = view.findViewById(R.id.searchView);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fillterList(newText);
                return true;
            }
        });

        recyclerView = view.findViewById(R.id.recView);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        pAdapter = new ProductsAdapter();

        recyclerViewCategory = view.findViewById(R.id.recViewCategory);
        recyclerViewCategory.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategory.setLayoutManager(linearLayoutManager);
        cAdapter = new CategoryAdapter(getActivity());

        Call<CategoryResponse> categoryResponseCall = RetrofitClientInstance.getApiService().getDataCategory();
        categoryResponseCall.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Gson gson = new Gson();
                Log.d("response category : ", gson.toJson(response.body()));

                if (response.isSuccessful()){
                    CategoryResponse categoryResponse = response.body();
                    dataCategoryResponseList = categoryResponse.getData();
                    recyclerViewCategory.setAdapter(cAdapter);
                    cAdapter.setCategoryList(dataCategoryResponseList);


                }else {
                    Toast.makeText(getActivity(), "Error : ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        Call<ProductsResponse> productsResponseCall = RetrofitClientInstance.getApiService().getDataProducts();
        productsResponseCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                Gson gson = new Gson();
                Log.d("response data : ", gson.toJson(response.body()));

                if (response.isSuccessful()) {
                    ProductsResponse productsResponse = response.body();
                    dataResponseList = productsResponse.getData();
                    recyclerView.setAdapter(pAdapter);
                    pAdapter.setList(dataResponseList);

                }else {
                    Toast.makeText(getActivity(), "error ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fillterList(String text) {
        List<DataResponse> filltereadList = new ArrayList<>();

        for (DataResponse dataResponse : dataResponseList){
            if (dataResponse.getAttributes().getName().toLowerCase().contains(text.toLowerCase())){
                filltereadList.add(dataResponse);
            }

            pAdapter.setFillteredList(filltereadList);

        }
    }
}