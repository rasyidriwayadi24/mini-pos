package com.example.minipos.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minipos.Constants;
import com.example.minipos.R;
import com.example.minipos.core.RetrofitClientInstance;
import com.example.minipos.core.adapter.MenuCategoryAdapter;
import com.example.minipos.core.network.response.product.DataResponse;
import com.example.minipos.core.network.response.product.ProductsResponse;
import com.example.minipos.core.network.response.product.category.CategoryResponse;
import com.example.minipos.core.network.response.product.category.DataCategoryResponse;
import com.example.minipos.core.network.response.product.category.Product;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodCategoryActivity extends AppCompatActivity {

    Button btnBackMenu;

    DataCategoryResponse dataCategoryResponse;
    String idCategory;

    RecyclerView recyclerViewMenuCategory;
    GridLayoutManager gridLayoutManagerMenuCategory;
    MenuCategoryAdapter mAdapter;

    List<DataResponse> dataResponseCategoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_menu_item);

        dataCategoryResponse = getIntent().getParcelableExtra(Constants.HASILMENUCATEGORY);
        idCategory = getIntent().getStringExtra(Constants.IDCATEGORY);

        btnBackMenu = findViewById(R.id.btnBack);
        btnBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerViewMenuCategory = findViewById(R.id.recViewMenuCategory);
        recyclerViewMenuCategory.setHasFixedSize(true);
        gridLayoutManagerMenuCategory = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerViewMenuCategory.setLayoutManager(gridLayoutManagerMenuCategory);
        mAdapter = new MenuCategoryAdapter();

        Call<ProductsResponse> productsResponseCategoryCall = RetrofitClientInstance.getApiService().getDataCategoryProduct(idCategory);
        productsResponseCategoryCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                Gson gson = new Gson();
                Log.d("menu category ", gson.toJson(response.body()));
                if (response.isSuccessful()){
                    ProductsResponse productsResponseCategory = response.body();
                    dataResponseCategoryList = productsResponseCategory.getData();
                    recyclerViewMenuCategory.setAdapter(mAdapter);
                    mAdapter.setDataCategory(dataResponseCategoryList);
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {

            }
        });


        if (getIntent().getExtras() != null){
        }
    }
}