package com.example.minipos.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.minipos.core.network.response.login.LoginBody;
import com.example.minipos.core.network.response.login.LoginResponse;
import com.example.minipos.R;
import com.example.minipos.core.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    EditText edt_name, edt_word;
    Button btn_l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edt_name = findViewById(R.id.edt_user);
        edt_word = findViewById(R.id.edt_pass);
        btn_l = findViewById(R.id.btn_login);

        btn_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edt_name.getText().toString().trim();
                String password = edt_word.getText().toString().trim();

                if (email.isEmpty()){
                    Toast.makeText(Login.this, "UserName / Email Is Required", Toast.LENGTH_SHORT).show();
                    edt_name.setError("UserName / Email Is Required");
                    return;
                } else if (password.isEmpty()){
                    Toast.makeText(Login.this, "Password is Required", Toast.LENGTH_SHORT).show();
                    edt_word.setError("Password is Required");
                    return;
                } else {
                    login();
                }
            }
        });
    }

    public void login(){
        LoginBody loginBody = new LoginBody();
        loginBody.setEmail(edt_name.getText().toString());
        loginBody.setPassword(edt_word.getText().toString());

        Call<LoginResponse> loginResponseCall = RetrofitClientInstance.getApiService().login(loginBody);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()){
                    Toast.makeText(Login.this, "Login Successful", Toast.LENGTH_SHORT).show();
                    LoginResponse loginResponse = response.body();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            startActivity(new Intent(Login.this, DashboardActivity.class).putExtra("data", loginResponse.getName()));
                        }
                    }, 100);

                } else{
                    Toast.makeText(Login.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(Login.this, "Throwable " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}