package com.example.minipos.core.network.response.product.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {

    @SerializedName("data")
    @Expose
    private List<DataCategoryResponse> data = null;

    public List<DataCategoryResponse> getData() {
        return data;
    }

    public void setData(List<DataCategoryResponse> data) {
        this.data = data;
    }
}
