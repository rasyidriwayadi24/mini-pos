package com.example.minipos.core.network.response.product.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataCategoryResponse implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("attributes")
    @Expose
    private Attributes attributes;
    @SerializedName("relationships")
    @Expose
    private Relationships relationships;

    protected DataCategoryResponse(Parcel in) {
        id = in.readString();
        type = in.readString();
    }

    public static final Creator<DataCategoryResponse> CREATOR = new Creator<DataCategoryResponse>() {
        @Override
        public DataCategoryResponse createFromParcel(Parcel in) {
            return new DataCategoryResponse(in);
        }

        @Override
        public DataCategoryResponse[] newArray(int size) {
            return new DataCategoryResponse[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Relationships getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(type);
    }
}
