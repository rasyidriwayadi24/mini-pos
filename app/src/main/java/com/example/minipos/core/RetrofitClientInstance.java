package com.example.minipos.core;

import androidx.annotation.NonNull;

import com.example.minipos.core.network.service.ApiService;
import com.example.minipos.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static Retrofit retrofit;

    //http://192.168.1.69:3000/api/v1/auth/login/

    public static Retrofit getInstance(){
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoyLCJleHAiOjE2NjM1NTgxNTB9.V-hra1Xn5ixIbUimwQU2WbXs0s5dLd5BJX3EZpp_O6qC2CjF8WJX328bnqLC35TmDsyvz_fASoMerf5dUZYAJA";
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static ApiService getApiService(){
        ApiService apiService = getInstance().create(ApiService.class);

        return apiService;
    }

}
