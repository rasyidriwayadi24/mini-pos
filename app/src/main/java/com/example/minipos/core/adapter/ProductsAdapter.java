package com.example.minipos.core.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minipos.Constants;
import com.example.minipos.R;
import com.example.minipos.core.network.response.product.DataResponse;
import com.example.minipos.ui.main.DetailProductActivity;

import java.text.Format;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    List<DataResponse> dataResponseList;
    public void setList(List<DataResponse> list){
        dataResponseList = list;
        notifyItemRangeInserted(0, list.size()-1);
    }
    public void setFillteredList(List<DataResponse> fillteredList){
        this.dataResponseList = fillteredList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, parent, false);
        ProductsViewHolder productsViewHolder = new ProductsViewHolder(view);
        return productsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder holder, int position) {
        holder.bind(dataResponseList.get(position));
        Glide.with(holder.itemView.getContext())
                .load(dataResponseList.get(position).getAttributes().getImage())
                .placeholder(R.drawable.loading)
                .error(R.drawable.icon_food_2)
                .into(holder.imgProduct);
    }

    @Override
    public int getItemCount() {
        return dataResponseList.size();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder {
        TextView tvNameProduct, tvPriceProduct;
        ImageView imgProduct;

        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameProduct = itemView.findViewById(R.id.tvMenuName);
            tvPriceProduct = itemView.findViewById(R.id.tvPrice);
            imgProduct = itemView.findViewById(R.id.imgMenu);
        }

        public void bind(DataResponse resultDataProducts) {
            String detailName = resultDataProducts.getAttributes().getName();
            tvNameProduct.setText(detailName);
            String detailPrice = resultDataProducts.getAttributes().getPrice();
            tvPriceProduct.setText(detailPrice);
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), DetailProductActivity.class);
                intent.putExtra(Constants.DETAILCATEGORY, resultDataProducts);
                String idDetail = resultDataProducts.getId();
                intent.putExtra(Constants.IDDETAILPRODUCT, idDetail);
                itemView.getContext().startActivity(intent);
            });
        }
    }
}
