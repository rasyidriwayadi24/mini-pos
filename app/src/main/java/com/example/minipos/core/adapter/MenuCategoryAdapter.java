package com.example.minipos.core.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minipos.R;
import com.example.minipos.core.network.response.product.DataResponse;
import com.example.minipos.core.network.response.product.category.DataCategoryResponse;

import java.util.List;

public class MenuCategoryAdapter extends RecyclerView.Adapter<MenuCategoryAdapter.MenuCategoryViewHolder> {

    List<DataResponse> dataResponseCategoryList;
    public void setDataCategory(List<DataResponse> list){
        dataResponseCategoryList = list;
        notifyItemRangeInserted(0, list.size()-1);
    }

    @NonNull
    @Override
    public MenuCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item, parent, false);
        MenuCategoryViewHolder menuCategoryViewHolder = new MenuCategoryViewHolder(view);
        return menuCategoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuCategoryViewHolder holder, int position) {
        holder.bind(dataResponseCategoryList.get(position));
        Glide.with(holder.itemView.getContext())
                .load(dataResponseCategoryList.get(position).getAttributes().getImage())
                .placeholder(R.drawable.loading)
                .error(R.drawable.icon_food_2)
                .into(holder.imgProductCategory);
    }

    @Override
    public int getItemCount() {
        return dataResponseCategoryList.size();
    }

    public class MenuCategoryViewHolder extends RecyclerView.ViewHolder {

        TextView tvNameProductCategory, tvPriceProductCategory;
        ImageView imgProductCategory;

        public MenuCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameProductCategory = itemView.findViewById(R.id.tvMenuName);
            tvPriceProductCategory = itemView.findViewById(R.id.tvPrice);
            imgProductCategory = itemView.findViewById(R.id.imgMenu);
        }

        public void bind(DataResponse resultDataCategory) {
            tvNameProductCategory.setText(resultDataCategory.getAttributes().getName());
            tvPriceProductCategory.setText(resultDataCategory.getAttributes().getPrice());
        }
    }
}
