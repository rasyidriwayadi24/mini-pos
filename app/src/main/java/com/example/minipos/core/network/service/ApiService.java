package com.example.minipos.core.network.service;

import com.example.minipos.core.network.response.login.LoginBody;
import com.example.minipos.core.network.response.login.LoginResponse;
import com.example.minipos.core.network.response.product.ProductsResponse;
import com.example.minipos.core.network.response.product.category.CategoryResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiService {

    @POST("/api/v1/auth/login/")
    Call<LoginResponse> login(@Body LoginBody loginBody);

    @GET("/api/v1/products")
    Call<ProductsResponse> getDataProducts();

    @GET("/api/v1/product_categories/{id_category}/products")
    Call<ProductsResponse> getDataCategoryProduct(
            @Path("id_category") String idCategory
    );

    @GET("/api/v1/products/{id_detail}")
    Call<ProductsResponse> getDataDetailProduct(
            @Path("id_detail") String idDetail
    );

    @GET("/api/v1/product_categories")
    Call<CategoryResponse> getDataCategory();

}
