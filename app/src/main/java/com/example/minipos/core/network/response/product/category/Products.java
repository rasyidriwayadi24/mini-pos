package com.example.minipos.core.network.response.product.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Products {
    @SerializedName("data")
    @Expose
    private List<DatasCategoryResponses> data = null;

    public List<DatasCategoryResponses> getData() {
        return data;
    }

    public void setData(List<DatasCategoryResponses> data) {
        this.data = data;
    }
}
