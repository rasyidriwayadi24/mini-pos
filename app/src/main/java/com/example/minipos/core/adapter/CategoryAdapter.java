package com.example.minipos.core.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minipos.Constants;
import com.example.minipos.R;
import com.example.minipos.core.network.response.product.category.DataCategoryResponse;
import com.example.minipos.ui.main.FoodCategoryActivity;

import java.util.List;
import java.util.Locale;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{

    private Context context;

    public CategoryAdapter(Context context) {
        this.context = context;
    }

    List<DataCategoryResponse> dataCategoryResponseList;
    public void setCategoryList(List<DataCategoryResponse> list){
        dataCategoryResponseList = list;
        notifyItemRangeInserted(0, list.size()-1);
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.bind(dataCategoryResponseList.get(position));
        Glide.with(holder.itemView.getContext())
                .load(dataCategoryResponseList.get(position).getAttributes().getImage())
                .placeholder(R.drawable.loading)
                .error(R.drawable.food)
                .into(holder.imageCategory);

    }

    @Override
    public int getItemCount() {
        return dataCategoryResponseList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{

        TextView tvNameCategory;
        ImageView imageCategory;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameCategory = itemView.findViewById(R.id.tvCategoryName);
            imageCategory = itemView.findViewById(R.id.imgCategory);
        }

        public void bind(DataCategoryResponse resultDataCategory) {
            String name = resultDataCategory.getAttributes().getName();
            tvNameCategory.setText(name);
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), FoodCategoryActivity.class);
                intent.putExtra(Constants.HASILMENUCATEGORY, resultDataCategory);
                String idCategory = resultDataCategory.getId();
                intent.putExtra(Constants.IDCATEGORY, idCategory);
                itemView.getContext().startActivity(intent);
            });
        }
    }
}
